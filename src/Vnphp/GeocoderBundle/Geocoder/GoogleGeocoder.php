<?php


namespace Vnphp\GeocoderBundle\Geocoder;

use Buzz\Browser;
use Vnphp\GeocoderBundle\Exception\EmptyResultsException;

class GoogleGeocoder implements GeocoderInterface
{
    /**
     * @var Browser
     */
    private $browser;

    /**
     * GoogleGeocoder constructor.
     * @param Browser $browser
     */
    public function __construct(Browser $browser)
    {
        $this->browser = $browser;
    }

    public function getLatLngByAddress($address)
    {
        $url = 'http://maps.googleapis.com/maps/api/geocode/json?' . http_build_query([
                'address' => $address,
            ]);
        $response = $this->browser->get($url);
        $data = json_decode($response->getContent());
        if (count($data->results) === 0) {
            throw new EmptyResultsException();
        }
        $lat = $data->results['0']->geometry->location->lat;
        $lng = $data->results['0']->geometry->location->lng;

        return [
            'lat' => $lat,
            'lng' => $lng,
        ];
    }
}
