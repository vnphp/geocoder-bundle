<?php

namespace Vnphp\GeocoderBundle\Geocoder;

interface GeocoderInterface
{
    public function getLatLngByAddress($address);
}
