<?php


namespace Vnphp\GeocoderBundle\Geocoder;

use Doctrine\Common\Cache\Cache;

class CacheableGeocoder implements GeocoderInterface
{
    /**
     * @var GeocoderInterface
     */
    private $delegate;

    /**
     * @var Cache
     */
    private $cache;

    /**
     * @var integer
     */
    private $ttl;

    /**
     * CacheableGeocoder constructor.
     * @param GeocoderInterface $delegate
     * @param Cache $cache
     * @param int $ttl
     */
    public function __construct(GeocoderInterface $delegate, Cache $cache, $ttl = 0)
    {
        $this->delegate = $delegate;
        $this->cache = $cache;
        $this->ttl = $ttl;
    }

    public function getLatLngByAddress($address)
    {
        $cacheKey = md5("geocoder:lat-lng-by-address:{$address}");
        $result = $this->cache->fetch($cacheKey);
        if (false === $result) {
            $result = $this->delegate->getLatLngByAddress($address);
            $this->cache->save($cacheKey, $result, $this->ttl);
        }
        return $result;
    }
}
