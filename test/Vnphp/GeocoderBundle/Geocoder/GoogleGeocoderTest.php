<?php

class GoogleGeocoderTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Vnphp\GeocoderBundle\Geocoder\GoogleGeocoder
     */
    private $geocoder;

    /**
     * @var \Buzz\Browser|\PHPUnit_Framework_MockObject_MockObject
     */
    private $browser;

    protected function setUp()
    {
        $this->browser = $this->createMock(\Buzz\Browser::class);
        $this->geocoder = new \Vnphp\GeocoderBundle\Geocoder\GoogleGeocoder($this->browser);
    }

    public function testGetLatLngByAddress()
    {
        $expected = [
            'lat' => 49.2214053,
            'lng' => 28.4135511,
        ];

        $apiData = [
            'results' => [
                [
                    'geometry' => [
                        'location' => $expected
                    ]
                ]
            ]
        ];

        $response = $this->createMock(\Buzz\Message\Response::class);
        $response->expects(static::once())
            ->method('getContent')
            ->will(static::returnValue(json_encode($apiData)));

        $this->browser->expects(static::once())
            ->method('get')
            ->will(static::returnValue($response));

        static::assertEquals($expected, $this->geocoder->getLatLngByAddress('г. Винница, пр-т Юности, 73а'));
    }

    public function testGetLatLngByAddressWithEmptyResults()
    {
        $this->expectException(\Vnphp\GeocoderBundle\Exception\EmptyResultsException::class);
        $apiData = [
            'results' => []
        ];

        $response = $this->createMock(\Buzz\Message\Response::class);
        $response->expects(static::once())
            ->method('getContent')
            ->will(static::returnValue(json_encode($apiData)));

        $this->browser->expects(static::once())
            ->method('get')
            ->will(static::returnValue($response));

        $this->geocoder->getLatLngByAddress('invalid address');
    }
}
